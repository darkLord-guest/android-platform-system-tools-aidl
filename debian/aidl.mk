NAME = aidl
SOURCES = main_java.cpp
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -Ldebian/out -laidl-common \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lbase

build: $(SOURCES)
	mkdir --parents debian/out
	$(CXX) $^ -o debian/out/$(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)